from .models import *
from django import forms

class RegistroProducto(forms.ModelForm):
    class Meta:
        model = Producto
        fields = ("nombre","precio")

class RegistroBebida(forms.ModelForm):
    class Meta:
        model = Bebida
        fields = ("nombre","precio")

class RegistroIngrediente(forms.ModelForm):
    class Meta:
        model = Ingrediente
        fields = ("nombre","precio","cantidad")
