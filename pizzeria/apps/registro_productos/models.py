from django.db import models

# Create your models here.

class Producto(models.Model):
    nombre = models.CharField(max_length=200)
    precio = models.IntegerField(default=0)

class Bebida(models.Model):
    nombre = models.CharField(max_length=200)
    precio = models.IntegerField(default=0)

class Ingrediente(models.Model):
    nombre = models.CharField(max_length=200)
    precio = models.IntegerField(default=0)
    cantidad = models.IntegerField(default=1)
