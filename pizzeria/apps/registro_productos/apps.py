from django.apps import AppConfig


class RegistroProductosConfig(AppConfig):
    name = 'registro_productos'
