from django.urls import path
from .views import *

urlpatterns = [
    path('', inicio, name='inicio'),
    path('pizza',registroPizza, name='pizza'),
    path('bebida', registroBebida, name='bebida'),
    path('ingrediente', registroIngrediente, name='ingrediente')
]
