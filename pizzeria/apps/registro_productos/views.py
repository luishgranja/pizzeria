from django.shortcuts import render
from .forms import *

# Create your views here.

def inicio(request):
	return render(request,'registro_productos/index.html', {"texto": 4})

def registroPizza(request):
	form = RegistroProducto()
	if (request.method=='POST'):
		form = RegistroProducto(request.POST)
		if form.is_valid():
			form.save()
	return render(request, 'registro_productos/registro_producto.html' , {"form" : form})

def registroBebida(request):
	form = RegistroBebida()
	if (request.method=='POST'):
		form = RegistroBebida(request.POST)
		if form.is_valid():
			form.save()
	return render(request, 'registro_productos/registro_bebida.html' , {"form_bebida" : form})

def registroIngrediente(request):
	form = RegistroIngrediente()
	if (request.method=='POST'):
		form = RegistroIngrediente(request.POST)
		if form.is_valid():
			form.save()
	return render(request, 'registro_productos/registro_ingrediente.html' , {"form_ingrediente" : form})
